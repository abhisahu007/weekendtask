﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StudentController : Controller
    {
        private readonly List<Student> studentList = new List<Student>();

        public StudentController()
        {
            studentList.Add(new Student
            {
                FirstName = "Abhi",
                LastName = "Sahu",
                Email = "abhi123@gmail.com",
                Age = 21,
                ID = 1,
                City = "Jaunpur",
                State = "UP",
                Address = "Jaunpur- 222001, UP INDIA"
            });

            studentList.Add(new Student
            {
                FirstName = "Test1",
                LastName = "1",
                Email = "test1@gmail.com",
                Age = 54,
                ID = 2,
                City = "XYZ",
                State = "MP",
                Address = "Bhopal-589654 INDIA"
            });

            studentList.Add(new Student
            {
                FirstName = "Test2",
                LastName = "2",
                Email = "test2@gmail.com",
                Age = 85,
                ID = 3,
                City = "OJHM",
                State = "HP",
                Address = "Manali"
            });
        }


        [HttpGet]
        [ApiVersion("1.0")]
        public IEnumerable<Student> GetStudent()
        {
            return ((IEnumerable<Student>)studentList);
        }

        [HttpGet("{id:int}")]
        [ApiVersion("1.0")]
        public Task<Student> GetStudentByID(int id)
        {
            var data = studentList.Find(data => data.ID == id);
            return Task.FromResult(data);
        }


        [HttpPost]
        [ApiVersion("2.0")]
        public IEnumerable<Student> AddStudent(Student StudentData)
        {
            studentList.Add(StudentData);
            return ((IEnumerable<Student>)studentList);
        }


        [HttpDelete("{id:int}")]
        [ApiVersion("2.0")]
        public IEnumerable<Student> DeleteStudent(int id)
        {
            studentList.Single(data => data.ID == id);
            return ((IEnumerable<Student>)studentList);
        }

        /*public IActionResult Index()
        {
            return View();
        }*/
    }
}
