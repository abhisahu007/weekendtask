﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Frontend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Frontend.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            _logger.LogInformation("Index called");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }


        public async Task<ActionResult> All()
        {
            string baseURL = "https://localhost:44383/";
            List<StudentModel> students = new List<StudentModel>();

            using(var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseURL);
                var request = await client.GetAsync("api/Student/?v=1");

                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    students = JsonConvert.DeserializeObject<List<StudentModel>>(result);
                }
                else
                {
                    _logger.LogInformation("Not TRUE");
                }
            }

            return View(students);
        }


        public IActionResult Add()
        {
            return View();
        }

        public ActionResult HandleAdd(StudentModel studentModel)
        {
            using(var client = new HttpClient())
            {
                string url = "https://localhost:44383/api/Student?v=2";
                client.BaseAddress = new Uri("https://localhost:44383/api/Student?v=2");


                var content = new StringContent(studentModel.ToString(), Encoding.UTF8, "application/json");
                var result = client.PostAsync(url, content).Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    _logger.LogError("POST Not success");
                }
            }

            return View("Index");
        }

        public IActionResult Search()
        {
            return View();
        }


        public async Task<ActionResult> HandleSearch(SearchModel searchModel)
        {
            string baseURL = "https://localhost:44383/";
            var unique = searchModel.ID;
            SearchModel singleData = new SearchModel();

            using( var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseURL);
                var request = await client.GetAsync($"api/Student/{unique}/?v=1");
                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    singleData = JsonConvert.DeserializeObject<SearchModel>(result);
                }
                else
                {
                    _logger.LogError("Get Element by ID failed");
                }
                return View(singleData);
            }

            return View("Index");
        }




        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
